import express, { Application } from 'express';
import frontend from './frontend';

const app: Application = express().disable('x-powered-by');

if (process.env.RAZZLE_PUBLIC_DIR) {
  app.use(express.static(process.env.RAZZLE_PUBLIC_DIR));
}

app.get('/*', frontend);

export default app;
