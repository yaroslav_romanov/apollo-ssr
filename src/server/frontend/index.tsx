/* eslint-disable no-console */
import { getDataFromTree } from '@apollo/react-ssr';
import { ChunkExtractor } from '@loadable/server';
import { ApolloClient, InMemoryCache } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { oneLineTrim } from 'common-tags';
import { RequestHandler } from 'express';
import path from 'path';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { FilledContext as HelmetFilledContext } from 'react-helmet-async';
import { StaticRouterContext } from 'react-router';
import { ServerStyleSheets } from '@material-ui/core/styles';
import { mapExtractor, mapHelmet } from './mappings';
import markup from './markup';
import Server from './Server';

interface FilledContext extends HelmetFilledContext {
  initial: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  router: StaticRouterContext;
}

// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

const statsFile = path.resolve('build/loadable-stats.json');
const entrypoints = ['client'];

const frontendRequestHandler: RequestHandler = async (req, res) => {
  try {
    const link = createHttpLink({
      fetch,
      headers: {
        cookie: req.header('Cookie'),
      },
      uri: process.env.RAZZLE_GRAPHQL_URL,
    });
    const apolloClient = new ApolloClient({
      cache: new InMemoryCache(),
      link,
      ssrMode: true,
      connectToDevTools: true,
    });
    const context: Record<string, any> = {}; // eslint-disable-line @typescript-eslint/no-explicit-any
    const chunkExtractor = new ChunkExtractor({ statsFile, entrypoints });
    const element = (
      <Server
        apolloClient={apolloClient}
        chunkExtractor={chunkExtractor}
        context={context}
        location={req.url}
      />
    );
    await getDataFromTree(element);
    const { helmet, router } = context as FilledContext;
    const apollo = apolloClient.extract();

    if (router.url) {
      res.redirect(router.statusCode || 301, router.url);
    } else {
      const sheets = new ServerStyleSheets();

      const html = markup({
        content: renderToString(sheets.collect(element)),
        extractor: mapExtractor(chunkExtractor),
        helmet: mapHelmet(helmet),
        initial: { apollo },
        css: sheets.toString(),
      });
      res.status(router.statusCode || 200).send(oneLineTrim(html));
    }
  } catch (error) {
    res.status(500);
    if (process.env.NODE_ENV === 'development') {
      res.json(error);
      console.log(error);
    } else {
      res.send('Server Error');
    }
  }
};

export default frontendRequestHandler;
