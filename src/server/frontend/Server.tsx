/* eslint-disable no-param-reassign */

import { ApolloProvider } from '@apollo/react-common';
import { ApolloClient, NormalizedCacheObject } from 'apollo-boost';
import { ChunkExtractorManager, ChunkExtractor } from '@loadable/server';
import { HelmetProvider } from 'react-helmet-async';
import React, { FC } from 'react';
import { StaticRouter, StaticRouterContext } from 'react-router';
import { ThemeProvider } from '@material-ui/core';
import App from '@/app';
import theme from '@/theme';

interface Props {
  apolloClient: ApolloClient<NormalizedCacheObject>;
  chunkExtractor: ChunkExtractor;
  context: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
  location: string;
}

const Server: FC<Props> = ({
  apolloClient,
  chunkExtractor,
  context,
  location,
}) => {
  const router: StaticRouterContext = {};
  context.router = router;

  return (
    <ChunkExtractorManager extractor={chunkExtractor}>
      <ApolloProvider client={apolloClient}>
        <HelmetProvider context={context}>
          <StaticRouter
            location={location}
            context={router}
          >
            <ThemeProvider theme={theme}>
              <App />
            </ThemeProvider>
          </StaticRouter>
        </HelmetProvider>
      </ApolloProvider>
    </ChunkExtractorManager>
  );
};

export default Server;
