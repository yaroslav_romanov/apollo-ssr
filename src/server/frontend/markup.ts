import { html } from 'common-tags';
import { HelmetData } from 'react-helmet';

interface Props {
  content: string;
  extractor: {
    linkTags: string;
    scriptTags: string;
    styleTags: string;
  };
  helmet: Record<keyof HelmetData, string>;
  initial: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  initialField?: string;
  css: string;
}

function markup({
  content,
  extractor,
  css,
  helmet,
  initial,
  initialField = '__APOLLO_CLIENT__',
}: Props) {
  const result: string = html`
    <!doctype html>
    <html ${helmet.htmlAttributes}>
      <head>
        <style id="jss-server-side">${css}</style>
        ${extractor.linkTags}
        ${extractor.styleTags}
        ${helmet.base}
        ${helmet.title}
        ${helmet.meta}
        ${helmet.link}
        ${helmet.style}
        ${helmet.script}
        ${helmet.noscript}
      </head>
      <body ${helmet.bodyAttributes}>
        <div id="root">${content}</div>
        <script>
          window.${initialField} = ${JSON.stringify(initial || {}).replace(/</g, '\\u003c')};
        </script>
        ${extractor.scriptTags}
      </body>
    </html>`;
  return result;
}

export default markup;
