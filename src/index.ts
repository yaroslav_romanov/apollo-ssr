/* eslint-disable no-console, global-require */

import http from 'http';

let app = require('./server').default;

let currentApp = app;

const server = http.createServer(app);
server.listen(process.env.PORT || 3000, () => console.log('🚀 started'));
server.on('error', (error) => console.error(error));

if (module.hot) {
  console.log('✅  Server-side HMR Enabled!');

  module.hot.accept('./server', () => {
    console.log('🔁  HMR Reloading `./server`...');

    try {
      app = require('./server').default;
      server.removeListener('request', currentApp);
      server.on('request', app);
      currentApp = app;
    } catch (error) {
      console.error(error);
    }
  });
}
