import React, { FC } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import useStyles from './styles';

interface PropsI {
  isLoading: boolean;
}

const LoadingLinear: FC<PropsI> = ({ isLoading }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {!!isLoading && <LinearProgress variant="query" />}
    </div>
  );
};

export default LoadingLinear;
