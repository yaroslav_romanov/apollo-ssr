// import { install } from 'offline-plugin/runtime';
import { loadableReady } from '@loadable/component';
import { ApolloClient, InMemoryCache } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';
import React, { useEffect } from 'react';
import { hydrate } from 'react-dom';
import { HelmetProvider } from 'react-helmet-async';
import { BrowserRouter } from 'react-router-dom';

import { ThemeProvider } from '@material-ui/core';
import App from '@/app';
import theme from '@/theme';

// eslint-disable-next-line no-underscore-dangle
const initialData = window.__APOLLO_CLIENT__ || {};

const apolloClient = new ApolloClient({
  cache: new InMemoryCache().restore(initialData.apollo),
  link: createHttpLink({
    uri: process.env.RAZZLE_GRAPHQL_URL,
  }),
  connectToDevTools: true,
});

const Main = () => {
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');

    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <ApolloProvider client={apolloClient}>
      <HelmetProvider>
        <BrowserRouter>
          <ThemeProvider theme={theme}>
            <App />
          </ThemeProvider>
        </BrowserRouter>
      </HelmetProvider>
    </ApolloProvider>
  );
};

loadableReady(() => {
  hydrate(
    <Main />,
    document.getElementById('root'),
  );
});

if (module.hot) {
  module.hot.accept();
}
