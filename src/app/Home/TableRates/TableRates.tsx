
// global imports
import React, {
  FC, useState, useEffect, useMemo,
} from 'react';
import { useQuery } from '@apollo/react-hooks';
import get from 'lodash/get';
import Paper from '@material-ui/core/Paper';
import {
  PagingState,
  CustomPaging,
  SortingState,
  FilteringState,
  Filter,
  Sorting,
  DataTypeProvider,
  TableColumnWidthInfo,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableFilterRow,
  PagingPanel,
  Toolbar,
  TableColumnVisibility,
  ColumnChooser,
  TableColumnReordering,
  DragDropProvider,
  TableColumnResizing,
} from '@devexpress/dx-react-grid-material-ui';

// local imports
import { LoadingLinear, ErrorMessage } from '@/ui';
import {
  RateParamsQueryI,
  ExtensionI,
  ColumnI,
  RatesResponseQueryI,
} from './types';

import GET_RATES from './query';
import initialOptions from './init';


const TableRates: FC = () => {
  // useMemo hooks
  const columns = useMemo<ColumnI[]>(() => initialOptions.columnsExtension.map(
    (column: ExtensionI) => ({ name: column.columnName, title: column.title }),
  ), []);
  const columnsOrder = useMemo<string[]>(() => initialOptions.columnsExtension.map(
    (column: ExtensionI) => column.columnName,
  ), []);
  const columnsHidden = useMemo<string[]>(() => initialOptions.columnsExtension
    .filter((column: ExtensionI) => column.hidden)
    .map((column: ExtensionI) => column.columnName),
  []);

  // useState hooks
  const [paramsQuery, setParamsQuery] = useState<RateParamsQueryI>({ skip: 0, take: 0 });
  const [hiddenColumns, setHiddenColumns] = useState(columnsHidden);
  const [columnOrder, setColumnOrder] = useState<string[]>(columnsOrder);
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [columnWidths, setColumnWidths] = useState<TableColumnWidthInfo[]>(initialOptions.columnsExtension);

  // apollo hooks
  const { data, loading: queryLoading, error } = useQuery<RatesResponseQueryI>(
    GET_RATES, { variables: paramsQuery, ssr: false },
  );

  // constants
  const rows = get(data, initialOptions.dataSource.data) || [];
  const totalCount = get(data, initialOptions.dataSource.totalCount) || 0;

  // useEffect hooks
  useEffect(() => {
    if (!data && !queryLoading) {
      setParamsQuery({ ...paramsQuery, skip: 0 });
    }
  }, [data, queryLoading, paramsQuery, setParamsQuery]);

  if (error) return <ErrorMessage message={error.message} />;

  // render component
  return (
    <Paper style={{ position: 'relative' }}>
      <LoadingLinear isLoading={queryLoading} />
      <Grid
        rows={rows}
        columns={columns}
      >
        <SortingState
          sorting={paramsQuery.sort}
          onSortingChange={handleChangeSorting}
          columnExtensions={initialOptions.columnsExtension}
        />
        <FilteringState
          filters={paramsQuery.filter}
          onFiltersChange={handleChangeFilter}
          columnExtensions={initialOptions.columnsExtension}
        />
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={handleChangePage}
          pageSize={paramsQuery.take}
        />
        <CustomPaging
          totalCount={totalCount}
        />
        <DragDropProvider />
        <Table />
        <TableColumnReordering
          order={columnOrder}
          onOrderChange={setColumnOrder}
        />
        {
          initialOptions.filterOperations.map((operation) => (
            <DataTypeProvider
              key={operation.id}
              for={operation.for}
              availableFilterOperations={operation.available}
            />
          ))
        }
        <TableColumnVisibility
          hiddenColumnNames={hiddenColumns}
          onHiddenColumnNamesChange={setHiddenColumns}
        />
        <TableColumnResizing
          resizingMode="nextColumn"
          columnWidths={columnWidths}
          onColumnWidthsChange={setColumnWidths}
        />
        <TableHeaderRow showSortingControls />
        <TableFilterRow showFilterSelector />
        <Toolbar />
        <ColumnChooser />
        <PagingPanel />
      </Grid>
    </Paper>
  );

  // handles
  function handleChangePage(page: number) {
    const skip = paramsQuery && paramsQuery.take ? paramsQuery.take * page : 0;
    setCurrentPage(page);
    setParamsQuery({ ...paramsQuery, skip });
  }

  function handleChangeSorting(sort: Sorting[]) {
    setParamsQuery({ ...paramsQuery, sort });
  }

  function handleChangeFilter(filter: Filter[]) {
    setParamsQuery({ ...paramsQuery, filter });
  }
};

export default TableRates;
