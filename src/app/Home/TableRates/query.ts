import gql from 'graphql-tag';

const GET_RATES = gql`
  query GetRates($currency: String! = "USD") {
    rates(currency: $currency){
      currency
      rate
      name
    }
  }
`;

export default GET_RATES;
