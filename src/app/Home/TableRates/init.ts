const initialOptions = {
  columnsExtension: [
    {
      columnName: 'currency',
      editingEnabled: false,
      filteringEnabled: false,
      hidden: false,
      required: true,
      sortingEnabled: false,
      title: 'Currency',
      type: 'text',
      width: 'auto',
    },
    {
      columnName: 'rate',
      editingEnabled: false,
      filteringEnabled: false,
      hidden: false,
      required: true,
      sortingEnabled: false,
      title: 'Rate',
      type: 'text',
      width: 'auto',
    },
    {
      columnName: 'name',
      editingEnabled: false,
      filteringEnabled: false,
      hidden: false,
      required: true,
      sortingEnabled: false,
      title: 'Name',
      type: 'text',
      width: 'auto',
    },
  ],
  dataSource: {
    data: 'rates',
    totalCount: 'rates.length',
  },
  filterOperations: [
    {
      id: 1,
      for: ['currency', 'rate', 'name'],
      available: ['contains', 'equal', 'startsWith'],
    },
  ],
};

export default initialOptions;
