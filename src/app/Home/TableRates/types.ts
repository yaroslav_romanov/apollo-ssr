import {
  Filter,
  Sorting,
} from '@devexpress/dx-react-grid';

export interface RateParamsQueryI {
  skip?: number;
  take?: number;
  sort?: Sorting[];
  filter?: Filter[];
}

export interface RateI {
  rate?: string;
  currency?: string;
  name?: string;
  __typename?: string;
}

export interface RatesResponseQueryI {
  rates: RateI[];
}

export interface ExtensionI {
  columnName: string;
  editingEnabled: boolean;
  filteringEnabled: boolean;
  hidden: boolean;
  required: boolean;
  sortingEnabled: boolean;
  title: string;
  type: string;
  width: string;
}

export interface ColumnI {
  name: string;
  title: string;
}
