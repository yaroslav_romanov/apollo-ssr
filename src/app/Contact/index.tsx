import { Helmet } from 'react-helmet-async';
import React, { FC } from 'react';

const Contact: FC = () => (
  <>
    <Helmet title="Contacts" />
    <div>Contact page</div>
  </>
);

export default Contact;
