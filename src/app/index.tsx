import '@/theme/global.scss';
import { CssBaseline } from '@material-ui/core';
import { Helmet } from 'react-helmet-async';
import { Route, Switch } from 'react-router-dom';
import loadable from '@loadable/component';
import React, { FC } from 'react';

const ContactPage = loadable(() => import('./Contact'));
const HomePage = loadable(() => import('./Home'));
const NotFoundPage = loadable(() => import('./NotFound'));

const App: FC = () => (
  <>
    <CssBaseline />
    <Helmet>
      <meta charSet="utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
    </Helmet>
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/contact" component={ContactPage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  </>
);

export default App;
