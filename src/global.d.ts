/* eslint-disable import/no-duplicates */

interface Window {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  __APOLLO_CLIENT__?: any;
}

declare module '*.module.css' {
  const exports: { [exportName: string]: string };
  export = exports;
}

declare module '*.module.scss' {
  const exports: { [exportName: string]: string };
  export = exports;
}

declare module '*.svg' {
  import { SFC, SVGProps, Component } from 'react';

  const content: string;

  export const ReactComponent: Component<SVGProps<SVGSVGElement>>;
  export default content;
}

declare module 'react-async-ssr' {
  import { ReactElement } from 'react';

  export function renderToStringAsync(element: ReactElement): Promise<string>;
}

declare module 'react-async-ssr/symbols';
